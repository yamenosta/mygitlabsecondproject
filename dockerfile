FROM nginx:alpine
COPY ./dist/GitLabProject :/var/www/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf